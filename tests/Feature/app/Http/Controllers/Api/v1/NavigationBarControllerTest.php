<?php

namespace Tests\Feature\app\Http\Controllers\Api\v1;

use Tests\TestCase;

class NavigationBarControllerTest extends TestCase
{
    /**
     * Scenario:
     * 取得影音首頁導航列設定
     */
    public function testGetBarSettings()
    {
        $response = $this->call('GET', '/gateway/api/v1/navigation');

        $this->assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('items', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertArrayHasKey('title', $content['items']);
        $this->assertArrayHasKey('image_url', $content['items']);
        $this->assertArrayHasKey('link', $content['items']);
        $this->assertSame('成功', $content['message']);
        $this->assertSame(200, $content['statusCode']);
    }
}
