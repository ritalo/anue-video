<?php

namespace Tests\Feature\app\Http\Controllers\Api\v1;

use App\Models\Category;
use App\Models\Vendor;
use App\Models\Video;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class VideoControllerTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    /**
     * Scenario:
     * 取得指定分類的影片列表
     */
    public function testGetList()
    {
        // Arrange data
        $vendor = factory(Vendor::class, 'anue')->create();
        $hotNews = factory(Category::class, 'videoHotNews')->create();
        $news = factory(Category::class, 'videoNews')->create([
            'parent_id' => $hotNews->id,
        ]);
        factory(Video::class)->create([
            'vendor_id' => $vendor->id,
            'category_id' => $news->id
        ]);

        // ACT
        $response = $this->call('GET', '/gateway/api/v1/video/category/video_hot_news');

        // Assertion
        $this->assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('items', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertArrayHasKey('current_page', $content['items']);
        $this->assertArrayHasKey('data', $content['items']);
        $this->assertArrayHasKey('first_page_url', $content['items']);
        $this->assertArrayHasKey('from', $content['items']);
        $this->assertArrayHasKey('last_page', $content['items']);
        $this->assertArrayHasKey('last_page_url', $content['items']);
        $this->assertArrayHasKey('next_page_url', $content['items']);
        $this->assertArrayHasKey('path', $content['items']);
        $this->assertArrayHasKey('per_page', $content['items']);
        $this->assertArrayHasKey('prev_page_url', $content['items']);
        $this->assertArrayHasKey('to', $content['items']);
        $this->assertArrayHasKey('total', $content['items']);
        $this->assertSame([[
            'id' => 1,
            'vendor_code' => 'anue',
            'vendor_video_id' => 'B4F0tX0KgYg',
            'title' => '全球經濟破底，為何股市不理？',
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/maxresdefault.jpg',
            ],
            'published_at' => 1561604406,
            'category_code' => 'video_news',
        ]], $content['items']['data']);
        $this->assertSame('成功', $content['message']);
        $this->assertSame(200, $content['statusCode']);

    }

    /**
     * Scenario:
     * 取得指定分類的影片列表-參數無效
     */
    public function testGetListResponse422()
    {
        // ACT
        $response = $this->call(
            'GET',
            '/gateway/api/v1/video/category/video_hot_news',
            ['page' => -1, 'limit' => 100,]
        );

        // Assertion
        $this->assertSame(422, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame([
            'page' =>[
                'The page must be at least 1.'
            ],
            'limit' => [
                'The limit may not be greater than 50.'
            ],
        ], $content['errors']);
        $this->assertSame('參數無效', $content['message']);
        $this->assertSame(422, $content['statusCode']);
    }

    /**
     * Scenario:
     * 取得指定分類的影片列表-該分類不存在
     */
    public function testGetListResponse404()
    {
        // ACT
        $response = $this->call('GET', '/gateway/api/v1/video/category/video_hot_news');

        // Assertion
        $this->assertSame(404, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame('此分類不存在', $content['message']);
        $this->assertSame(404, $content['statusCode']);
    }

    /**
     * Scenario:
     * 取得單則影片內容
     */
    public function testGetOne()
    {
        // Arrange data
        $vendor = factory(Vendor::class, 'anue')->create();
        $hotNews = factory(Category::class, 'videoHotNews')->create();
        $news = factory(Category::class, 'videoNews')->create([
            'parent_id' => $hotNews->id,
        ]);
        $video = factory(Video::class)->create([
            'vendor_id' => $vendor->id,
            'category_id' => $news->id
        ]);

        // ACT
        $response = $this->call('GET', '/gateway/api/v1/video/' . $video->id);

        // Assertion
        $this->assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('items', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame([
            'id' => 1,
            'title' => '全球經濟破底，為何股市不理？',
            'vendor_video_id' => 'B4F0tX0KgYg',
            'published_at' => 1561604406,
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/B4F0tX0KgYg/maxresdefault.jpg',
            ],
            'vendor_code' => 'anue',
            'category_code' => 'video_news',
        ], $content['items']);
        $this->assertSame('成功', $content['message']);
        $this->assertSame(200, $content['statusCode']);
    }

    /**
     * Scenario:
     * 取得單則影片內容-參數無效
     */
    public function testGetOneResponse422()
    {
        // ACT
        $response = $this->call('GET', '/gateway/api/v1/video/cnyescnyes');

        // Assertion
        $this->assertSame(422, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('errors', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame([
            'video_id' =>[
                'The video id must be an integer.'
            ],
        ], $content['errors']);
        $this->assertSame('參數無效', $content['message']);
        $this->assertSame(422, $content['statusCode']);
    }

    /**
     * Scenario:
     * 取得單則影片內容-影音不存在
     */
    public function testGetOneResponse404()
    {
        // ACT
        $response = $this->call('GET', '/gateway/api/v1/video/1');

        // Assertion
        $this->assertSame(404, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame('影音不存在', $content['message']);
        $this->assertSame(404, $content['statusCode']);
    }
}
