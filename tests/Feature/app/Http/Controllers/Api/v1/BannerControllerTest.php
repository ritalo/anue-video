<?php

namespace Tests\Feature\app\Http\Controllers\Api\v1;

use App\Models\Banner;
use App\Models\BannerSetting;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BannerControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Scenario：
     * 取得首頁 Banner 設定
     */
    public function testGetByType()
    {
        $banner = factory(Banner::class)->create();
        factory(BannerSetting::class)->create(['banner_id' => $banner->id]);

        $response = $this->call('GET', '/gateway/api/v1/banners', ['type' => 1]);
        $this->assertSame(200, $response->getStatusCode());
        $content = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('items', $content);
        $this->assertArrayHasKey('message', $content);
        $this->assertArrayHasKey('statusCode', $content);
        $this->assertSame([[
            'type' => 1,
            'position' => 1,
            'banner' => [
                'id' => 1,
                'title' => '購買學習課程, 鉅亨就請你喝咖啡',
                'subtitle' => '快來獲取專屬於您的咖啡',
                'button_word' => '按此查看活動辦法',
                'link' => 'https://www.cnyes.com/',
                'image_app' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
                'image_web' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
            ],
        ]], $content['items']);
        $this->assertSame('成功', $content['message']);
        $this->assertSame(200, $content['statusCode']);
    }

    /**
     * Scenario：
     * 取得首頁 Banner 設定-參數無效
     */
    public function testGetByTypeResponse422()
    {
        $banner = factory(Banner::class)->create();
        factory(BannerSetting::class)->create(['banner_id' => $banner->id]);

        $response = $this->call('GET', '/gateway/api/v1/banners', ['type' => 'fakeType']);
        $this->assertSame(422, $response->getStatusCode());
        $this->assertSame([
            'message' => '參數無效',
            'errors' => [
                'type' => ['The type must be an integer.'],
            ],
            'statusCode' => 422,
        ], json_decode($response->getContent(), true));
    }
}
