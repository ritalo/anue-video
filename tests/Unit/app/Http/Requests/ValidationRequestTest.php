<?php

namespace Tests\Unit\app\Http\Requests;

use App\Http\Requests\ValidationRequest;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class ValidationRequestTest extends TestCase
{
    use WithoutMiddleware;
    use MockObjectTrait;

    protected $request;

    public function setUp(): void
    {
        parent::setUp();
        $this->request = new ValidationRequest();
    }

    public function testAuthorizePassed()
    {
        $this->assertTrue($this->request->authorize());
    }

    public function testRulesPassed()
    {
        $attributes = [
            'category_code' => 'video_news',
            'limit' => 10,
            'page' => 1,
        ];

        $this->call('GET', '/gateway/api/v1/video/category/video_news');

        $rules = $this->request->rules();
        $validator = Validator::make($attributes, $rules);
        $this->assertTrue($validator->passes());
    }

    public function testRulesFailed()
    {
        $attributes = [
            'limit' => '我應該要是數字，但我是字串',
            'page'  => '我應該要是數字，但我還是字串',
        ];

        $this->call('GET', '/gateway/api/v1/video/category/video_news');

        $rules = $this->request->rules();
        $validator = Validator::make($attributes, $rules);
        $this->assertFalse($validator->passes());
    }

    /**
     * @expectedException \Illuminate\Validation\ValidationException
     */
    public function testFailedValidation()
    {
        $this->call('GET', '/gateway/api/v1/video/category/video_news');
        $attributes = [
            'limit' => '我應該要是數字，但我是字串',
            'page'  => '我應該要是數字，但我還是字串',
        ];

        $rules = $this->request->rules();
        $validator = Validator::make($attributes, $rules);

        $this->reflectMethod(ValidationRequest::class, 'failedValidation')->invoke($this->request, $validator);
    }

    public function testRuleForHasPathParametersAPI()
    {
        $categoryCode = 'video_news';

        $expectedParameters = [
            'category_code' => $categoryCode,
        ];

        $this->call('GET', '/gateway/api/v1/video/category/'. $categoryCode);

        $attributes = app(Request::class)->route()->parameters();
        $rules = $this->request->rules();

        $validator = Validator::make($attributes, $rules);
        $this->assertSame($expectedParameters, $attributes);
        $this->assertTrue($validator->passes());
    }
}
