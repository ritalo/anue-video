<?php

namespace Tests\Unit\app\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\VendorRepository;
use App\Repositories\VideoRepository;
use App\Services\UpdateAnueVideoService;
use CnyesLogger\CnyesLogger;
use Google_Client;
use Illuminate\Support\Facades\Config;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class UpdateAnueVideoServiceTest extends TestCase
{
    use MockObjectTrait;

    private $client;
    private $videoRepository;
    private $vendorRepository;
    private $categoryRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = $this->getMockObject(Google_Client::class);
        $this->videoRepository = $this->getMockObject(VideoRepository::class, ['updateOrCreate']);
        $this->vendorRepository = $this->getMockObject(VendorRepository::class, ['getByCode']);
        $this->categoryRepository = $this->getMockObject(CategoryRepository::class, []);
        $this->service = $this->getMockObjectWithArgs(
            UpdateAnueVideoServiceStub::class,
            [
                $this->client,
                $this->videoRepository,
                $this->vendorRepository,
                $this->categoryRepository,
            ],
            [
                'getAnueVendorId',
                'getByPlaylist',
                'updateByPlaylist',
            ]
        );
    }

    public function testUpdate()
    {
        Config::set('category.anue_playlist_mapping', [
            'videoNews' => [
                'PLWCmRE5W4ttZNf_u7GbRczaYoiMrk4DXC',
            ],
        ]);
        $this->categoryRepository->expects($this->once())->method('getByCode')->with('videoNews')->willReturn([
            'id' => 2,
            'display_name' => '影音新聞',
            'code' => 'videoNews',
        ]);
        $service = $this->getMockObjectWithArgs(
            UpdateAnueVideoServiceStub::class,
            [
                $this->client,
                $this->videoRepository,
                $this->vendorRepository,
                $this->categoryRepository,
            ],
            ['updateByPlaylist']
        );
        $service->expects($this->once())->method('updateByPlaylist')->with(2, 'PLWCmRE5W4ttZNf_u7GbRczaYoiMrk4DXC')->willReturn(null);
        $this->assertNull($service->update());
    }

    public function testUpdateByPlaylist()
    {
        $categoryId = 1;
        $playlistId = 'PLWCmRE5W4ttab3Phe516a1ZxXmwlap6NI';
        $service = $this->getMockObjectWithArgs(
            UpdateAnueVideoServiceStub::class,
            [
                $this->client,
                $this->videoRepository,
                $this->vendorRepository,
                $this->categoryRepository,
            ],
            ['getAnueVendorId', 'getByPlaylist']
        );
        $service->expects($this->once())->method('getAnueVendorId')->with()->willReturn($vendorId = 1);
        $service->expects($this->once())->method('getByPlaylist')->with($playlistId)->willReturn([[
            'kind' => 'youtube#playlistItem',
            'etag' => '"Bdx4f4ps3xCOOo1WZ91nTLkRZ_c/5uEeEwJV_JZB05CAP1Vo2SmzVao"',
            'id' => 'UExXQ21SRTVXNHR0Wk5mX3U3R2JSY3phWW9pTXJrNERYQy40NzZCMERDMjVEN0RFRThB',
            'snippet' => [
                'publishedAt' => '2019-06-27T07:40:53.000Z',
                'channelId' => 'UCHzw1et1X2fdz1VsXLO8nlw',
                'title' => '除權息旺季 高殖利率個股成為股期熱門標的｜影音新聞｜Anue鉅亨',
                'description' => '每年除權息旺季期間，不少投資人會藉由股票期貨進行避險，或者放大獲利的效果，選股就成為了投資重點。',
                'thumbnails' => [
                    'default' => [
                        'url' => 'https://i.ytimg.com/vi/42r2bzUhiDE/default.jpg',
                        'width' => 120,
                        'height' => 90,
                    ],
                ],
                'channelTitle' => 'Anue鉅亨',
                'playlistId' => $playlistId,
                'position' => 0,
                'resourceId' => [
                    'kind' => 'youtube#video',
                    'videoId' => '42r2bzUhiDE',
                ],
            ],
            'contentDetails' => [
                'videoId' => '42r2bzUhiDE',
                'videoPublishedAt' => '2019-06-27T07:40:51.000Z',
            ],
        ]]);
        $this->videoRepository->expects($this->once())->method('updateOrCreate')->with(
            [
                'category_id' => $categoryId,
                'vendor_id' => $vendorId,
                'vendor_video_id' => '42r2bzUhiDE',
            ],
            [
                'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
                'description' => '每年除權息旺季期間，不少投資人會藉由股票期貨進行避險，或者放大獲利的效果，選股就成為了投資重點。',
                'published_at' => '2019-06-27 07:40:51',
            ]
        )->willReturn([
            'id' => 1,
            'category_id' => $categoryId,
            'vendor_id' => $vendorId,
            'vendor_video_id' => '42r2bzUhiDE',
            'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
            'description' => '每年除權息旺季期間，不少投資人會藉由股票期貨進行避險，或者放大獲利的效果，選股就成為了投資重點。',
            'published_at' => '2019-06-27 07:40:51',
        ]);
        CnyesLogger::shouldReceive('errorReport')->never();

        $this->assertNull($service->updateByPlaylist($categoryId, $playlistId));
    }

    public function testUpdateByPlaylistWithLogger()
    {
        $categoryId = 2;
        $playlistId = 'PLWCmRE5W4ttab3Phe516a1ZxXmwlap6NI';
        $service = $this->getMockObjectWithArgs(
            UpdateAnueVideoServiceStub::class,
            [
                $this->client,
                $this->videoRepository,
                $this->vendorRepository,
                $this->categoryRepository,
            ],
            ['getAnueVendorId', 'getByPlaylist']
        );
        $service->expects($this->once())->method('getAnueVendorId')->with()->willReturn($vendorId = 1);
        $service->expects($this->once())->method('getByPlaylist')->with($playlistId)->willReturn([[]]);
        $this->videoRepository->expects($this->never())->method('updateOrCreate');
        CnyesLogger::shouldReceive('errorReport')->once()->andReturnNull();

        $service->updateByPlaylist($categoryId, $playlistId);
    }

    public function testGetAnueVendorId()
    {
        $this->vendorRepository->expects($this->once())->method('getByCode')->with('anue')->willReturn([
            'id' => 1,
            'display_name' => '鉅亨',
            'code' => 'anue',
        ]);
        $service = new UpdateAnueVideoServiceStub(
            $this->client,
            $this->videoRepository,
            $this->vendorRepository,
            $this->categoryRepository
        );
        $this->assertSame(1, $service->getAnueVendorId());
    }

    /**
     * @expectedException \UnexpectedValueException
     * @expectedExceptionMessage Anue vendor is not exists
     */
    public function testGetAnueVendorIdUnexpectedValueException()
    {
        $this->vendorRepository->expects($this->once())->method('getByCode')->with('anue')->willReturn([]);
        $service = new UpdateAnueVideoServiceStub(
            $this->client,
            $this->videoRepository,
            $this->vendorRepository,
            $this->categoryRepository
        );
        $service->getAnueVendorId();
    }
}

//@codingStandardsIgnoreStart
class UpdateAnueVideoServiceStub extends UpdateAnueVideoService
{
    //@codingStandardsIgnoreEnd
    public function updateByPlaylist(
        int $categoryId,
        string $playlistId
    ): void {
        parent::updateByPlaylist($categoryId, $playlistId);
    }

    public function getByPlaylist(string $playlistId): array
    {
        return parent::getByPlaylist($playlistId);
    }

    public function getAnueVendorId(): int
    {
        return parent::getAnueVendorId();
    }
}
