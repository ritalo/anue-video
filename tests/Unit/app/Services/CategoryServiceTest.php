<?php

namespace Tests\Unit\app\Services;

use App\Repositories\CategoryRepository;
use App\Services\CategoryService;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class CategoryServiceTest extends TestCase
{
    use MockObjectTrait;

    private $categoryRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->categoryRepository = $this->getMockObject(CategoryRepository::class, [
            'getByCode',
            'getChildren',
            'listVideos',
        ]);
    }

    public function testListVideoByCategoryCode()
    {
        $this->categoryRepository->expects($this->once())->method('getByCode')->with('video_hot_news')->willReturn([
            'id' => 1,
            'display_name' => '焦點新聞',
            'code' => 'video_hot_news',
        ]);
        $this->categoryRepository->expects($this->once())->method('getChildren')->with(1)->willReturn([[
            'id' => 2,
            'display_name' => '影音新聞',
            'code' => 'video_news',
        ]]);
        $this->categoryRepository->expects($this->once())->method('listVideos')->with([1, 2])->willReturn([[
            'id' => 1,
            'category_id' => 2,
            'category_code' => 'video_news',
            'vendor_id' => 1,
            'vendor_code' => 'anue',
            'vendor_video_id' => '42r2bzUhiDE',
            'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
            'description' => '每年除權息旺季期間，不少投資人會藉由股票期貨進行避險，或者放大獲利的效果，選股就成為了投資重點。',
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/42r2bzUhiDE/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/42r2bzUhiDE/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/42r2bzUhiDE/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/42r2bzUhiDE/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/42r2bzUhiDE/maxresdefault.jpg',
            ],
            'published_at' => 1561564800,
        ]]);

        $service = new CategoryService($this->categoryRepository);
        $result = $service->listVideoByCategoryCode('video_hot_news', 1, 20);
        $this->assertArrayHasKey('current_page', $result);
        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('first_page_url', $result);
        $this->assertArrayHasKey('from', $result);
        $this->assertArrayHasKey('last_page', $result);
        $this->assertArrayHasKey('last_page_url', $result);
        $this->assertArrayHasKey('next_page_url', $result);
        $this->assertArrayHasKey('path', $result);
        $this->assertArrayHasKey('per_page', $result);
        $this->assertArrayHasKey('prev_page_url', $result);
        $this->assertArrayHasKey('to', $result);
        $this->assertArrayHasKey('total', $result);
        $this->assertSame([[
            'id' => 1,
            'vendor_code' => 'anue',
            'vendor_video_id' => '42r2bzUhiDE',
            'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/42r2bzUhiDE/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/42r2bzUhiDE/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/42r2bzUhiDE/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/42r2bzUhiDE/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/42r2bzUhiDE/maxresdefault.jpg',
            ],
            'published_at' => 1561564800,
            'category_code' => 'video_news',
        ]], $result['data']);
    }

    /**
     * @expectedException \App\Exceptions\NotFoundException
     * @expectedExceptionMessage 分類不存在
     */
    public function testListVideoByCategoryCodeWithNotFoundException()
    {
        $this->categoryRepository->expects($this->once())->method('getByCode')->with('video_hot_news')->willReturn([]);
        $this->categoryRepository->expects($this->never())->method('getChildren');
        $this->categoryRepository->expects($this->never())->method('listVideos');

        $service = new CategoryService($this->categoryRepository);
        $service->listVideoByCategoryCode('video_hot_news', 1, 20);
    }
}
