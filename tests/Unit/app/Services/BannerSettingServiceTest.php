<?php

namespace Tests\Unit\app\Services;

use App\Repositories\BannerSettingRepository;
use App\Services\BannerSettingService;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class BannerSettingServiceTest extends TestCase
{
    use MockObjectTrait;

    private $settingRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->settingRepository = $this->getMockObject(BannerSettingRepository::class, [
            'getByType',
        ]);
    }

    public function testGetByType()
    {
        $type = 1;
        $this->settingRepository->expects($this->once())->method('getByType')->with($type)->willReturn([[
            'position' => 1,
            'type' => 1,
            'banner' => [
                'id' => 2,
                'title' => '購買學習課程, 鉅亨就請你喝咖啡',
                'subtitle' => '快來獲取專屬於您的咖啡',
                'button_word' => '按此查看活動辦法',
                'link' => 'https://www.cnyes.com/',
                'image_web' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
                'image_app' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
                'created_at' => 1559491200,
                'updated_at' => 1559491200,
                'deleted_at' => null,
            ],
            'created_at' => 1561564800,
            'updated_at' => 1561564800,
            'deleted_at' => null,
        ]]);

        $service = new BannerSettingService($this->settingRepository);
        $this->assertSame([[
            'type' => 1,
            'position' => 1,
            'banner' => [
                'id' => 2,
                'title' => '購買學習課程, 鉅亨就請你喝咖啡',
                'subtitle' => '快來獲取專屬於您的咖啡',
                'button_word' => '按此查看活動辦法',
                'link' => 'https://www.cnyes.com/',
                'image_web' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
                'image_app' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
            ],
        ]], $service->getByType($type));
    }
}
