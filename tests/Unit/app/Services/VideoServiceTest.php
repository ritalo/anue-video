<?php

namespace Tests\Unit\app\Services;

use App\Repositories\VideoRepository;
use App\Services\VideoService;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class VideoServiceTest extends TestCase
{
    use MockObjectTrait;

    private $videoRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->videoRepository = $this->getMockObject(VideoRepository::class, [
            'find',
        ]);
    }

    public function testGetById()
    {
        $videoId = 1;
        $this->videoRepository->expects($this->once())->method('find')->with($videoId)->willReturn([
            'id' => 1,
            'category_id' => 2,
            'category_code' => 'video_news',
            'vendor_id' => 1,
            'vendor_code' => 'anue',
            'vendor_video_id' => '42r2bzUhiDE',
            'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
            'description' => '每年除權息旺季期間，不少投資人會藉由股票期貨進行避險，或者放大獲利的效果，選股就成為了投資重點。',
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/42r2bzUhiDE/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/42r2bzUhiDE/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/42r2bzUhiDE/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/42r2bzUhiDE/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/42r2bzUhiDE/maxresdefault.jpg',
            ],
            'published_at' => 1561564800,
        ]);

        $service = new VideoService($this->videoRepository);
        $this->assertSame([
            'id' => $videoId,
            'category_code' => 'video_news',
            'vendor_code' => 'anue',
            'vendor_video_id' => '42r2bzUhiDE',
            'title' => '除權息旺季 高殖利率個股成為股期熱門標的',
            'thumbnails' => [
                'default' => 'https://i.ytimg.com/vi/42r2bzUhiDE/default.jpg',
                'medium' => 'https://i.ytimg.com/vi/42r2bzUhiDE/mqdefault.jpg',
                'high' => 'https://i.ytimg.com/vi/42r2bzUhiDE/hqdefault.jpg',
                'standard' => 'https://i.ytimg.com/vi/42r2bzUhiDE/sddefault.jpg',
                'maxres' => 'https://i.ytimg.com/vi/42r2bzUhiDE/maxresdefault.jpg',
            ],
            'published_at' => 1561564800,
        ], $service->getById($videoId));
    }

    /**
     * @expectedException \App\Exceptions\NotFoundException
     * @expectedExceptionMessage 影音不存在
     */
    public function testGetByIdNotFoundException()
    {
        $videoId = 1;
        $this->videoRepository->expects($this->once())->method('find')->with($videoId)->willReturn([]);

        $service = new VideoService($this->videoRepository);
        $service->getById($videoId);
    }
}
