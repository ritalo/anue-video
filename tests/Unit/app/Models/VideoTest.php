<?php

namespace Tests\Unit\app\Models;

use App\Models\Category;
use App\Models\Vendor;
use App\Models\Video;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class VideoTest extends TestCase
{
    use MockObjectTrait;

    public function testGetThumbnailsAttribute()
    {
        $video = $this->getMockObject(Video::class);
        $video->vendor_video_id = 'DsFDD-S-baI';

        $vendor = $this->getMockObject(Vendor::class);
        $vendor->code = 'anue';
        $video->vendor = $vendor;

        $this->assertSame([
            'default' => 'https://i.ytimg.com/vi/DsFDD-S-baI/default.jpg',
            'medium' => 'https://i.ytimg.com/vi/DsFDD-S-baI/mqdefault.jpg',
            'high' => 'https://i.ytimg.com/vi/DsFDD-S-baI/hqdefault.jpg',
            'standard' => 'https://i.ytimg.com/vi/DsFDD-S-baI/sddefault.jpg',
            'maxres' => 'https://i.ytimg.com/vi/DsFDD-S-baI/maxresdefault.jpg',
        ], $video->thumbnails);
    }

    public function testGetVendorCodeAttribute()
    {
        $video = $this->getMockObject(Video::class);
        $vendor = $this->getMockObject(Vendor::class);
        $vendor->code = 'anue';
        $video->vendor = $vendor;

        $this->assertSame('anue', $video->vendor_code);
    }

    public function testGetVendorCodeAttributeEmpty()
    {
        $video = $this->getMockObject(Video::class);

        $this->assertEmpty($video->vendor_code);
    }

    public function testGetCategoryCodeAttribute()
    {
        $video = $this->getMockObject(Video::class);
        $category = $this->getMockObject(Category::class);
        $category->code = 'video_program';
        $video->category = $category;

        $this->assertSame('video_program', $video->category_code);
    }

    public function testGetCategoryCodeAttributeEmpty()
    {
        $video = $this->getMockObject(Video::class);

        $this->assertEmpty($video->category_code);
    }
}
