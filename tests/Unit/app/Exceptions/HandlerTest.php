<?php

namespace Tests\Unit\app\Exceptions;

use App\Exceptions\Handler;
use CnyesLogger\CnyesLogger;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use InvalidArgumentException;
use PDOException;
use Tests\TestCase;
use Mockery as m;

class HandlerTest extends TestCase
{
    public function testDontReportProperty()
    {
        $stub = app(ExceptionHandlerStub::class);
        $this->assertEquals(
            [
                ValidationException::class,
                AuthorizationException::class,
                ModelNotFoundException::class,
                HttpException::class,
            ],
            $stub->dontReport
        );
    }

    public function testShouldReportExceptionThroughLogger()
    {
        $exception = new InvalidArgumentException;
        CnyesLogger::shouldReceive('getDontReport')->once();
        CnyesLogger::shouldReceive('errorReportRemotely')->once();

        $stub = $this->getMockBuilder(ExceptionHandlerStub::class)
            ->setConstructorArgs([$this->app])
            ->setMethods(['shouldReport'])
            ->getMock();
        $stub->expects($this->once())->method('shouldReport')->with($exception)->willReturn(true);
        $stub->report($exception);
    }

    public function testShouldNotReportExceptionThroughLogger()
    {
        $exception = new InvalidArgumentException;
        CnyesLogger::shouldReceive('getDontReport')->once();
        CnyesLogger::shouldReceive('errorReportRemotely')->never();

        $stub = $this->getMockBuilder(ExceptionHandlerStub::class)
            ->setConstructorArgs([$this->app])
            ->setMethods(['shouldReport'])
            ->getMock();
        $stub->expects($this->once())->method('shouldReport')->with($exception)->willReturn(false);
        $stub->report($exception);
    }

    public function testRenderHttpErrors()
    {
        $stub = new ExceptionHandlerStub($this->app);
        $result = $stub->renderHttpErrors(new UnauthorizedHttpException('challenge', 'UnauthorizedHttpException'));

        $this->assertEquals(401, $result->getStatusCode());
        $this->assertEquals(
            [
                'message' => 'UnauthorizedHttpException',
                'statusCode' => 401
            ],
            json_decode($result->getContent(), true)
        );

        return $result;
    }

    public function testRenderUnknownErrors()
    {
        $stub = new ExceptionHandlerStub($this->app);
        $result = $stub->renderUnknownErrors(new PDOException('host=fakeHost;password=fakePassword'));

        $this->assertEquals(500, $result->getStatusCode());
        $this->assertEquals(
            [
                'message' => '未知的錯誤',
                'statusCode' => 500
            ],
            json_decode($result->getContent(), true)
        );

        return $result;
    }

    /**
     * @depends testRenderHttpErrors
     */
    public function testRenderInstanceOfHttpErrors($response)
    {
        $stub = $this->getMockBuilder(ExceptionHandlerStub::class)
            ->setConstructorArgs([$this->app])
            ->setMethods(['renderHttpErrors'])
            ->getMock();
        $stub->expects($this->once())->method('renderHttpErrors')->willReturn($response);

        $this->assertInstanceOf(
            JsonResponse::class,
            $stub->render(m::mock(Request::class), new UnauthorizedHttpException('challenge', 'UnauthorizedHttpException'))
        );
    }

    /**
     * @depends testRenderUnknownErrors
     */
    public function testRenderInstanceOfUnknownErrors($response)
    {
        $stub = $this->getMockBuilder(ExceptionHandlerStub::class)
            ->setConstructorArgs([$this->app])
            ->setMethods(['renderUnknownErrors'])
            ->getMock();
        $stub->expects($this->once())->method('renderUnknownErrors')->willReturn($response);

        $this->assertInstanceOf(
            JsonResponse::class,
            $stub->render(m::mock(Request::class), new PDOException('host=fakeHost;password=fakePassword'))
        );
    }
}

// @codingStandardsIgnoreStart
class ExceptionHandlerStub extends Handler
{
    // @codingStandardsIgnoreEnd
    public $dontReport;
}
