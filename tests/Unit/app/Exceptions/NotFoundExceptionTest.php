<?php

namespace Tests\Unit\app\Exceptions;

use App\Exceptions\NotFoundException;
use Illuminate\Http\Response;
use Tests\TestCase;

class NotFoundExceptionTest extends TestCase
{
    public function testConstruct()
    {
        $exception = new NotFoundException('resource no found!');
        $this->assertSame(Response::HTTP_NOT_FOUND, $exception->getStatusCode());
        $this->assertSame('resource no found!', $exception->getMessage());
    }
}
