<?php

namespace Tests\Unit\app\Repositories;

use App\Models\Category;
use App\Repositories\CategoryRepository;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class CategoryRepositoryTest extends TestCase
{
    use MockObjectTrait;

    public function testGetByCode()
    {
        $code = 'videoProgram';

        $model = $this->getMockObject(Category::class, ['where', 'first']);
        $model->id = 1;
        $model->display_name = '影音節目';
        $model->code = $code;
        $model->expects($this->once())->method('where')->with('code', $code)->willReturnSelf();
        $model->expects($this->once())->method('first')->with()->willReturnSelf();

        $repository = new CategoryRepository($model);
        $this->assertSame([
            'id' => 1,
            'display_name' => '影音節目',
            'code' => $code,
        ], $repository->getByCode($code));
    }

    public function testGetByCodeWithEmpty()
    {
        $code = 'videoProgram';

        $model = $this->getMockObject(Category::class, ['where', 'first']);
        $model->expects($this->once())->method('where')->with('code', $code)->willReturnSelf();
        $model->expects($this->once())->method('first')->with()->willReturn(null);

        $repository = new CategoryRepository($model);
        $this->assertEmpty($repository->getByCode($code));
    }

    public function testGetChildren()
    {
        $categoryId = 1;

        $model = $this->getMockObject(Category::class, ['where', 'get']);
        $model->expects($this->once())->method('where')->with('parent_id', $categoryId)->willReturnSelf();
        $model->expects($this->once())->method('get')->with()->willReturn(collect([[
            'id' => 2,
            'parent_id' => $categoryId,
            'display_name' => 'Allen 看世界',
        ]]));

        $repository = new CategoryRepository($model);
        $this->assertSame([[
            'id' => 2,
            'parent_id' => $categoryId,
            'display_name' => 'Allen 看世界',
        ]], $repository->getChildren($categoryId));
    }
}
