<?php

namespace Tests\Unit\app\Repositories;

use App\Models\Vendor;
use App\Repositories\VendorRepository;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class VendorRepositoryTest extends TestCase
{
    use MockObjectTrait;

    public function testGetByCode()
    {
        $code = 'anue';

        $model = $this->getMockObject(Vendor::class, ['where', 'first']);
        $model->id = 1;
        $model->display_name = '鉅亨';
        $model->code = $code;
        $model->expects($this->once())->method('where')->with('code', $code)->willReturnSelf();
        $model->expects($this->once())->method('first')->with()->willReturnSelf();

        $repository = new VendorRepository($model);
        $this->assertSame([
            'id' => 1,
            'display_name' => '鉅亨',
            'code' => $code,
        ], $repository->getByCode($code));
    }

    public function testGetByCodeWithEmpty()
    {
        $code = 'anue';

        $model = $this->getMockObject(Vendor::class, ['where', 'first']);
        $model->expects($this->once())->method('where')->with('code', $code)->willReturnSelf();
        $model->expects($this->once())->method('first')->with()->willReturn(null);

        $repository = new VendorRepository($model);
        $this->assertEmpty($repository->getByCode($code));
    }
}
