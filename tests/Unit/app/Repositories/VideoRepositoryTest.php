<?php

namespace Tests\Unit\app\Repositories;

use App\Models\Video;
use App\Repositories\VideoRepository;
use Test\Unit\Concerns\MockObjectTrait;
use Tests\TestCase;

class VideoRepositoryTest extends TestCase
{
    use MockObjectTrait;

    public function testUpdateOrCreate()
    {
        $keys = ['id' => 1];
        $values = ['display_name' => '鉅亨影音'];
        $model = $this->getMockObject(Video::class, ['updateOrCreate']);
        $model->expects($this->once())->method('updateOrCreate')->with($keys, $values)->willReturnSelf();

        $repository = new VideoRepository($model);
        $this->assertSame([
            'thumbnails' => [],
            'vendor_code' => '',
            'category_code' => '',
            'vendor' => null,
            'category' => null,
        ], $repository->updateOrCreate($keys, $values));
    }

    public function testFind()
    {
        $videoId = 1;
        $model = $this->getMockObject(Video::class, ['find']);
        $model->expects($this->once())->method('find')->with($videoId)->willReturnSelf();

        $repository = new VideoRepository($model);
        $this->assertSame([
            'thumbnails' => [],
            'vendor_code' => '',
            'category_code' => '',
            'vendor' => null,
            'category' => null,
        ], $repository->find($videoId));
    }

    public function testFindEmpty()
    {
        $videoId = 1;
        $model = $this->getMockObject(Video::class, ['find']);
        $model->expects($this->once())->method('find')->with($videoId)->willReturn(null);

        $repository = new VideoRepository($model);
        $this->assertEmpty($repository->find($videoId));
    }
}
