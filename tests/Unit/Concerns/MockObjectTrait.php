<?php

namespace Test\Unit\Concerns;

use ReflectionClass;

/**
 * Trait MockObjectTrait
 * @package Test\Unit\Concerns
 */
trait MockObjectTrait
{
    /**
     * The method used to get mock object instance
     *
     * @param string $class
     * @param array|null $method If null, no methods will be replaced.
     *
     * @return \PHPUnit\Framework\MockObject\MockObject
     */

    private function getMockObject(string $class, ?array $methods = null)
    {
        return $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * The method used to get mock object instance with construct arguments
     *
     * @param string $class
     * @param array $args
     * @param array|null $method If null, no methods will be replaced.
     *
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    private function getMockObjectWithArgs(string $class, array $args, ?array $methods = null)
    {
        return $this->getMockBuilder($class)
            ->setConstructorArgs($args)
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * 設定可以取得特定 Reflection Class 的 Method
     *
     * @param  string $className
     * @param  string $methodName
     *
     * @throws \ReflectionException
     *
     * @return \ReflectionMethod
     */
    protected function reflectMethod($className, $methodName)
    {
        $class = new ReflectionClass($className);

        $method = $class->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
