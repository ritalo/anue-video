<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Vendor;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(Vendor::class, 'anue', function () {
    return [
        'display_name' => '鉅亨',
        'code' => 'anue',
    ];
});
