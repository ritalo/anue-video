<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Video;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Video::class, function () {
    return [
        'title' => '全球經濟破底，為何股市不理？',
        'description' => '今年以來聯準會 (Fed) 態度有別於去年強悍升息四碼的作風，態度明顯轉向鴿派，引領了全球股市強勢反彈，而面對市場質疑 Fed 偏離傳統「泰勒規則」太遠的批判聲浪，Fed 也對此提出了現代版「泰勒規則」回應。',
        'vendor_video_id' => 'B4F0tX0KgYg',
        'published_at' => '2019-06-27 03:00:06',
    ];
});
