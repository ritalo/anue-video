<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->defineAs(Category::class, 'videoProgram', function () {
    return [
        'display_name' => '影音節目',
        'code' => 'video_program',
    ];
});

$factory->defineAs(Category::class, 'allenSeeTheWorld', function () {
    return [
        'display_name' => 'Allen看世界',
        'code' => 'allen_see_the_world',
    ];
});

$factory->defineAs(Category::class, 'financialManagement', function () {
    return [
        'display_name' => '理財芳程式',
        'code' => 'financial_management',
    ];
});

$factory->defineAs(Category::class, 'anueFund', function () {
    return [
        'display_name' => '鉅亨FUND大鏡',
        'code' => 'anue_fund',
    ];
});

$factory->defineAs(Category::class, 'hkStockLecture', function () {
    return [
        'display_name' => '港股大講堂',
        'code' => 'hk_stock_lecture',
    ];
});

$factory->defineAs(Category::class, 'teaTimeAfterClose', function () {
    return [
        'display_name' => '盤後下午茶',
        'code' => 'tea_time_after_close',
    ];
});

$factory->defineAs(Category::class, 'videoHotNews', function () {
    return [
        'display_name' => '焦點新聞',
        'code' => 'video_hot_news',
    ];
});

$factory->defineAs(Category::class, 'videoNews', function () {
    return [
        'display_name' => '影音新聞',
        'code' => 'video_news',
    ];
});
