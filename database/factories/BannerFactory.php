<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Banner;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Banner::class, function () {
    return [
        'title' => '購買學習課程, 鉅亨就請你喝咖啡',
        'subtitle' => '快來獲取專屬於您的咖啡',
        'button_word' => '按此查看活動辦法',
        'link' => 'https://www.cnyes.com/',
        'image_web' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
        'image_app' => 'https://cimg.cnyes.cool/prod/anue-video/banner/2/m/2ef8afa8effcd6fdde14516dcb4801d5.jpg',
    ];
});
