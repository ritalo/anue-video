<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\BannerSetting;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(BannerSetting::class, function () {
    return [
        'position' => 1,
        'type' => 1,
    ];
});
