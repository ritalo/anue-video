<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $program = factory(Category::class, 'videoProgram')->create();
        factory(Category::class, 'allenSeeTheWorld')->create(['parent_id' => $program->id]);
        factory(Category::class, 'financialManagement')->create(['parent_id' => $program->id]);
        factory(Category::class, 'anueFund')->create(['parent_id' => $program->id]);
        factory(Category::class, 'hkStockLecture')->create(['parent_id' => $program->id]);
        factory(Category::class, 'teaTimeAfterClose')->create(['parent_id' => $program->id]);

        $hotNews = factory(Category::class, 'videoHotNews')->create();
        factory(Category::class, 'videoNews')->create(['parent_id' => $hotNews->id]);
    }
}
