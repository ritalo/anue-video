<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->comment('主標題');
            $table->string('subtitle')->default('')->comment('副標題');
            $table->string('button_word')->default('')->comment('按鈕文字');
            $table->string('link')->comment('前往連結');
            $table->string('image_app')->comment('App背景圖連結');
            $table->string('image_web')->comment('Web背景圖連結');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
