#!/usr/bin/env bash

"${COMPOSE_EXEC_CMD[@]}" app composer global require hirak/prestissimo
"${COMPOSE_EXEC_CMD[@]}" app composer install
if ! [ -e ${PARENT_DIR}/.env ]; then
    "${COMPOSE_EXEC_CMD[@]}" app composer run-script -- post-root-package-install
fi
