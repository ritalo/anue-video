#!/usr/bin/env bash

EXIT_CODE=0

"${COMPOSE_EXEC_CMD[@]}" app ./vendor/bin/phplint --configuration=builds/phplint.yml --no-cache
"${COMPOSE_EXEC_CMD[@]}" app ./vendor/bin/phpcs --standard=./builds/phpcs.xml || EXIT_CODE=$((EXIT_CODE + 2))
"${COMPOSE_EXEC_CMD[@]}" app ./vendor/bin/phpunit -c ./builds/phpunit.xml || EXIT_CODE=$((EXIT_CODE + 3))

if [[ $CI_MODE == true ]]; then
    APP_ID=$(get_container_id app)
    docker cp $APP_ID:/cnyes_code/builds/reports/phpunit ./builds/reports
    docker cp $APP_ID:/cnyes_code/storage/logs ./storage
fi

exit $EXIT_CODE
