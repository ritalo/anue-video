#!/usr/bin/env bash

# support to execute one collection for development
if [ -n "$2" ]; then
    collection=${2:0:-5}
    "${COMPOSE_RUN_CMD[@]}" newman run \
        /cnyes_code/tests/postman/collection/${collection}.json \
        --reporters cli,html \
        --reporter-html-export /cnyes_code/builds/reports/newman/${collection}.html \
        -e /cnyes_code/tests/postman/environment/ci.json
    exit 0
fi

EXIT_CODE=0

for collection in ${PARENT_DIR}/tests/postman/collection/*.json; do
    collection=${collection##*/}
    collection=${collection:0:-5}
    "${COMPOSE_RUN_CMD[@]}" newman run \
        /cnyes_code/tests/postman/collection/${collection}.json \
        --reporters cli,html \
        --reporter-html-export /cnyes_code/builds/reports/newman/${collection}.html \
        -e /cnyes_code/tests/postman/environment/ci.json || EXIT_CODE=$((EXIT_CODE + 1))
done

if [[ $CI_MODE == true ]]; then
    APP_ID=$(get_container_id app)
    docker cp $APP_ID:/cnyes_code/builds/reports/newman ./builds/reports
    docker cp $APP_ID:/cnyes_code/storage/logs ./storage
fi

exit $EXIT_CODE
