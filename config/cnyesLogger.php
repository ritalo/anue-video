<?php

return [
    // CnyesLogger default logger driver
    'default' => 'sentry',

    //Set list of the exception types that should not be reported.
    'dontReport' => [
        \Illuminate\Validation\ValidationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
    ],

    //Set error log report enable or disable
    'enabled' => env('CNYES_LOGGER_ERRLOG_REMOTE_ENABLED', false),

    //CnyesLogger remotes
    'remotes' => [
        'sentry' => [
            'driver' => 'sentry'
        ]
    ]
];
