<?php

return [
    /**
     * 導航列第四個選項設定
     * 文件：https://cnyesrd.atlassian.net/wiki/spaces/PS/pages/680427529
     */
    'settings' => [
        'title' => '',
        'image_url' => '',
        'link' => '',
    ],
];
