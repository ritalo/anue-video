<?php

return [
    /**
     * Template of thumbnail
     * For example,
     * [
     *     {vendor_code} => [
     *         // template
     *     ],
     *     ...
     * ]
     */
    'thumbnail_template' => [
        'anue' => [
            'default' => 'https://i.ytimg.com/vi/{vendorVideoId}/default.jpg',
            'medium' => 'https://i.ytimg.com/vi/{vendorVideoId}/mqdefault.jpg',
            'high' => 'https://i.ytimg.com/vi/{vendorVideoId}/hqdefault.jpg',
            'standard' => 'https://i.ytimg.com/vi/{vendorVideoId}/sddefault.jpg',
            'maxres' => 'https://i.ytimg.com/vi/{vendorVideoId}/maxresdefault.jpg',
        ],
    ],
];
