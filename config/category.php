<?php

return [
    /**
     * The Mapping between Anue Youtube channel's playlist id and category
     * For example,
     * {category_code} => [{playlist id1}, {playlist id2}, ...]
     */
    'anue_playlist_mapping' => [
        // Allen 看世界
        'allen_see_the_world' => [
            'PLWCmRE5W4ttYKQG8DCixoN-iSrspnd1Hh',
        ],
        // 理財芳程式
        'financial_management' => [
            'PLWCmRE5W4ttYbGU-hMdmsNzTeb4S4OKh0',
        ],
        // 鉅亨FUND大鏡
        'anue_fund' => [
            'PLWCmRE5W4ttab3Phe516a1ZxXmwlap6NI',
        ],
        // 港股大講堂
        'hk_stock_lecture' => [
            'PLWCmRE5W4ttbVi8VtKbVYPZaUkF1x37c1',
        ],
        // 盤後下午茶
        'tea_time_after_close' => [
            'PLWCmRE5W4ttZ3CjkeSQ_oFhvGQu6RzpcN'
        ],
        // 影音新聞
        'video_news' => [
            'PLWCmRE5W4ttZNf_u7GbRczaYoiMrk4DXC',
        ],
    ],
];
