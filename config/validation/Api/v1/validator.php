<?php

return [
    'video' => [
        'getList' => [
            'category_code' => ['required', 'string'],
            'page' => ['integer', 'min:1'],
            'limit' => ['integer', 'min:3', 'max:50'],
        ],
        'getOne' => [
            'video_id' => ['required', 'integer'],
        ],
    ],
    'banner' => [
        'getByType' => [
            'type' => ['required', 'integer', 'min:1'],
        ],
    ],
];
