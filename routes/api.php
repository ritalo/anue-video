<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    /** health check */
    Route::get('health', function () {
        return [
            'environment' => env('APP_ENV'),
            'version' => '5.8'
        ];
    });

    /** 取得分類影音列表 */
    Route::get('video/category/{category_code}', 'Api\v1\VideoController@getList');

    /** 取得單則影音 */
    Route::get('video/{video_id}', 'Api\v1\VideoController@getOne');

    /** 取得導航列設定 */
    Route::get('navigation', 'Api\v1\NavigationBarController@getBarSettings');

    /** 取得Banner設定 */
    Route::get('banners', 'Api\v1\BannerController@getByType');
});

Route::prefix('v1/internal')->group(function () {
    /** 更新Banner */
    Route::post('banner', 'Api\v1\Internal\BannerController@update');
});

