<?php

namespace App\Services;

use App\Repositories\BannerSettingRepository;
use Illuminate\Support\Arr;

class BannerSettingService
{
    /**
     * Repository of BannerSettings
     *
     * @var BannerSettingRepository $settingRepository
     */
    protected $settingRepository;

    /**
     * Create a new instance
     *
     * @param BannerSettingRepository $settingRepository
     */
    public function __construct(BannerSettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    /**
     * Get settings by the given type
     *
     * @param int $type
     *
     * @return array
     */
    public function getByType(int $type): array
    {
        return array_map(function ($item) {
            return [
                'type' => (int) Arr::get($item, 'type'),
                'position' => (int) Arr::get($item, 'position'),
                'banner' => Arr::only(
                    Arr::get($item, 'banner', []),
                    ['id', 'title', 'subtitle', 'button_word', 'link', 'image_app', 'image_web']
                )
            ];
        }, $this->settingRepository->getByType($type));
    }
}
