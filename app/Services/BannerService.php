<?php

namespace App\Services;

use Aws\Laravel\AwsFacade;
use Aws\S3\S3Client;

class BannerService
{
    /**
     */
    public function upload()
    {
        $s3Client = new S3Client([
            'region' => config('aws.region'),
            'version' => config('aws.version'),
        ]);
        $bucket = config('aws.s3.buckets.banner');

        // Construct an array of conditions for policy
        $options = [
            ['bucket' => $bucket],
            ['starts-with', '$key', 123321 . '.'],
        ];

        // Optional: configure expiration time string
        $expires = '+10 minutes';

        $postObject = new \Aws\S3\PostObjectV4(
            $s3Client,
            $bucket,
            [],
            $options,
            $expires
        );

        $result = [
            // Get attributes to set on an HTML form, e.g., action, method, enctype
            'url' => $postObject->getFormAttributes(),
            // Get form input fields. This will include anything set as a form input in
            // the constructor, the provided JSON policy, your AWS access key ID, and an
            // auth signature.
            'fields' => $postObject->getFormInputs()
        ];

        return $result;
        //var_dump($s3);exit;
        /*$s3->putObject(array(
            'Bucket'     => 'YOUR_BUCKET',
            'Key'        => 'YOUR_OBJECT_KEY',
            'SourceFile' => '/the/path/to/the/file/you/are/uploading.ext',
        ));*/
    }
}
