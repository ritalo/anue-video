<?php

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Repositories\VideoRepository;
use Illuminate\Support\Arr;

class VideoService
{
    /**
     * Repository of video model
     *
     * @var VideoRepository $videoRepository
     */
    protected $videoRepository;

    /**
     * Create a new instance
     *
     * @param VideoRepository $videoRepository
     */
    public function __construct(VideoRepository $videoRepository)
    {
        $this->videoRepository = $videoRepository;
    }

    /**
     * Get a video by the specific video id
     *
     * @param int $videoId
     *
     * @throws \App\Exceptions\NotFoundException
     *
     * @return array
     */
    public function getById(int $videoId): array
    {
        $video = $this->videoRepository->find($videoId);
        if (empty($video)) {
            throw new NotFoundException('影音不存在');
        }

        return Arr::only($video, [
            'id',
            'vendor_code',
            'vendor_video_id',
            'title',
            'thumbnails',
            'published_at',
            'category_code',
        ]);
    }
}
