<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\VendorRepository;
use App\Repositories\VideoRepository;
use Carbon\Carbon;
use CnyesLogger\CnyesLogger;
use Illuminate\Support\Arr;
use Google_Client;
use Google_Service_YouTube;
use UnexpectedValueException;

class UpdateAnueVideoService
{
    /**
     * Google service
     *
     * @var Google_Service_YouTube $googleService
     */
    protected $googleService;

    /**
     * Video repository
     *
     * @var VideoRepository $videoRepository
     */
    protected $videoRepository;

    /**
     * Vendor respository
     *
     * @var VendorRepository $vendorRepository
     */
    protected $vendorRepository;

    /**
     * Category repository
     *
     * @var CategoryRepository $categoryRepository
     */
    protected $categoryRepository;

    /**
     * Create an instance
     *
     * @param Google_Client $client
     * @param VideoRepository $videoRepository
     * @param VendorRepository $vendorRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(
        Google_Client $client,
        VideoRepository $videoRepository,
        VendorRepository $vendorRepository,
        CategoryRepository $categoryRepository
    ) {
        $client->setDeveloperKey(config('google_client.api_key', ''));
        $this->googleService = new Google_Service_YouTube($client);
        $this->videoRepository = $videoRepository;
        $this->vendorRepository = $vendorRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Update Anue video into database
     */
    public function update(): void
    {
        $mapping = config('category.anue_playlist_mapping', []);
        foreach ($mapping as $categoryCode => $playlist) {
            $categoryId = Arr::get($this->categoryRepository->getByCode($categoryCode), 'id');
            foreach ($playlist as $playlistId) {
                $this->updateByPlaylist($categoryId, $playlistId);
            }
        }
    }

    /**
     * Update videos which in Youtube's playlist to the specific category
     *
     * @param int $categoryId
     * @param string $playlistId
     */
    protected function updateByPlaylist(
        int $categoryId,
        string $playlistId
    ): void {
        $vendorId = $this->getAnueVendorId();
        $videoList = $this->getByPlaylist($playlistId);
        foreach ($videoList as $video) {
            try {
                if (!Arr::has($video, ['contentDetails.videoId', 'contentDetails.videoPublishedAt'])) {
                    throw new UnexpectedValueException('Missing necessary array keys');
                }
                $this->videoRepository->updateOrCreate(
                    [
                        'category_id' => $categoryId,
                        'vendor_id' => $vendorId,
                        'vendor_video_id' => $video['contentDetails']['videoId'],
                    ],
                    [
                        'title' => Arr::get(explode('｜', Arr::get($video, 'snippet.title', '')), 0, ''),
                        'description' => Arr::get($video, 'snippet.description', ''),
                        'published_at' => Carbon::parse($video['contentDetails']['videoPublishedAt'])->toDateTimeString(),
                    ]
                );
            } catch (UnexpectedValueException $exception) {
                CnyesLogger::errorReport($exception);
            }
        }
    }

    /**
     * Get videos by Youtube's playlist id
     *
     * @param string $playlistId
     *
     * @return array
     */
    protected function getByPlaylist(string $playlistId): array
    {
        $result = [];
        do {
            $queryParams = [
                'maxResults' => 50,
                'playlistId' => $playlistId,
                'pageToken' => $nextPageToken ?? '',
            ];

            $response = $this->googleService->playlistItems->listPlaylistItems(
                'snippet,contentDetails',
                $queryParams
            );
            $result[] = json_decode(json_encode($response->getItems()), true);
            $nextPageToken = $response->getNextPageToken();
        } while (!empty($nextPageToken));

        // Reduce a N-dimension array to a simple array
        return array_merge(...$result);
    }

    /**
     * Get Anue vendor id
     *
     * @throws UnexpectedValueException
     *
     * @return int
     */
    protected function getAnueVendorId(): int
    {
        $id = Arr::get($this->vendorRepository->getByCode('anue'), 'id');
        if (null === $id) {
            throw new UnexpectedValueException('Anue vendor is not exists');
        }

        return $id;
    }
}
