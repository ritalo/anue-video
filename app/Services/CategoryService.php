<?php

namespace App\Services;

use App\Exceptions\NotFoundException;
use App\Repositories\CategoryRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;

class CategoryService
{
    /**
     * Repository of category model
     *
     * @var CategoryRepository $categoryRepository
     */
    protected $categoryRepository;

    /**
     * Create an instance
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * List video by category code
     * TODO: Add cache
     *
     * @param string $categoryCode
     * @param int $page
     * @param int $limit
     *
     * @throws \App\Exceptions\NotFoundException
     *
     * @return array
     */
    public function listVideoByCategoryCode(string $categoryCode, int $page, int $limit): array
    {
        $parentId = Arr::get($this->categoryRepository->getByCode($categoryCode), 'id');
        if (null === $parentId) {
            throw new NotFoundException('此分類不存在');
        }

        $categoryIds = array_merge([$parentId], array_column($this->categoryRepository->getChildren($parentId), 'id'));
        $videoList = $this->categoryRepository->listVideos($categoryIds);
        $data = array_map(function ($item) {
            return [
                'id' => $item['id'],
                'vendor_code' => $item['vendor_code'],
                'vendor_video_id' => $item['vendor_video_id'],
                'title' => $item['title'],
                'thumbnails' => $item['thumbnails'],
                'published_at' => $item['published_at'],
                'category_code' => $item['category_code'],
            ];
        }, \array_slice($videoList, (1 === $page) ? 0 : (($page - 1) * $limit) + 1, $page * $limit));

        $paginator = new LengthAwarePaginator(
            $data,
            \count($videoList),
            $limit,
            $page,
            [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
                'query' => ['page' => $page],
            ]
        );
        return $paginator->toArray();
    }
}
