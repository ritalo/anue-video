<?php

namespace App\Console;

use App\Services\UpdateAnueVideoService;
use Illuminate\Console\Command;

class UpdateAnueVideos extends Command
{
    protected $signature = 'update:anue-video';
    protected $description = '更新 Anue Youtube 頻道的播放清單至資料表';

    /**
     * Command handler
     */
    public function handle(): void
    {
        $this->info('Ready to update Anue video from Youtube');
        $this->info('Doing...');
        $service = app(UpdateAnueVideoService::class);
        $service->update();

        $this->info('Update successfully!');
    }
}
