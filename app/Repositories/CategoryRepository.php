<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    /**
     * Eloquent model for this repository
     *
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * Create a new instance
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get record by category code
     *
     * @param string $code
     *
     * @return array
     */
    public function getByCode(string $code): array
    {
        $model = $this->model->where('code', $code)->first();

        return (null === $model) ? [] : $model->toArray();
    }

    /**
     * Get records which it's parent_id is the given value
     *
     * @param int $categoryId
     *
     * @return array
     */
    public function getChildren(int $categoryId): array
    {
        return $this->model->where('parent_id', $categoryId)->get()->toArray();
    }

    /**
     * Get video list by category id
     *
     * @param array $categoryId
     *
     * @return array
     */
    public function listVideos(array $categoryId): array
    {
        $categories = $this->model::with('videos')
            ->whereIn('id', $categoryId)
            ->get()
            ->pluck('videos')
            ->sortByDesc('published_at')
            ->toArray();

        // Reduce a N-dimension array to a simple array
        return array_merge(...$categories);
    }
}
