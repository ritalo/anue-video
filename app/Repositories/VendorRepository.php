<?php

namespace App\Repositories;

use App\Models\Vendor;

class VendorRepository
{
    /**
     * Eloquent model for this repository
     *
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * Create a new instance
     *
     * @param Vendor $vendor
     */
    public function __construct(Vendor $vendor)
    {
        $this->model = $vendor;
    }

    /**
     * Get record vy vendor code
     *
     * @param string $code
     *
     * @return array
     */
    public function getByCode(string $code): array
    {
        $model = $this->model->where('code', $code)->first();

        return (null === $model) ? [] : $model->toArray();
    }
}
