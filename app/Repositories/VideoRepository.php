<?php

namespace App\Repositories;

use App\Models\Video;

class VideoRepository
{
    /**
     * Eloquent model for this repository
     *
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * Create a new instance
     *
     * @param Video $video
     */
    public function __construct(Video $video)
    {
        $this->model = $video;
    }

    /**
     * Update or create record
     *
     * @param array $keys
     * @param array $values
     *
     * @return array
     */
    public function updateOrCreate(array $keys, array $values): array
    {
        return $this->model->updateOrCreate($keys, $values)->toArray();
    }

    /**
     * Find video by id
     *
     * @param int $videoId
     *
     * @return array
     */
    public function find(int $videoId): array
    {
        $video = $this->model->find($videoId);

        return empty($video) ? [] : $video->toArray();
    }
}
