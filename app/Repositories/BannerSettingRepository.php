<?php

namespace App\Repositories;

use App\Models\BannerSetting;

class BannerSettingRepository
{
    /**
     * Eloquent model for this repository
     *
     * @var \Illuminate\Database\Eloquent\Model $model
     */
    protected $model;

    /**
     * Create a new instance
     *
     * @param BannerSetting $bannerSetting
     */
    public function __construct(BannerSetting $bannerSetting)
    {
        $this->model = $bannerSetting;
    }

    /**
     * Get records by type
     *
     * @param int $type
     *
     * @return array
     */
    public function getByType(int $type): array
    {
        return $this->model::with('banner')
            ->where('type', $type)
            ->orderBy('position', 'asc')
            ->get()
            ->toArray();
    }
}
