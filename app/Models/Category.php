<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * @{inheritdoc}
     */
    protected $guarded = ['id'];

    /**
     * @{inheritdoc}
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    /**
     * Create relationship between category and video
     */
    public function videos(): HasMany
    {
        return $this->hasMany(Video::class, 'category_id', 'id');
    }
}
