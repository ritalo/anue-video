<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class Video extends Model
{
    use SoftDeletes;

    /**
     * @{inheritdoc}
     */
    protected $guarded = ['id'];

    /**
     * @{inheritdoc}
     */
    protected $casts = [
        'published_at' => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
        'deleted_at' => 'timestamp',
    ];

    /**
     * @{inheritdoc}
     */
    protected $appends = [
        'thumbnails',
        'vendor_code',
        'category_code',
    ];

    /**
     * Create relationship between video and vendor
     *
     * @return HasOne
     */
    public function vendor(): HasOne
    {
        return $this->hasOne(Vendor::class, 'id', 'vendor_id');
    }

    /**
     * Create relationship between video and category
     *
     * @return HasOne
     */
    public function category(): HasOne
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    /**
     * Get the attribute of thumbnails
     *
     * @return array
     */
    public function getThumbnailsAttribute(): array
    {
        $config = config('video.thumbnail_template', []);
        if (!empty($this->vendor) && 'anue' === $this->vendor->code) {
            $template = Arr::get($config, 'anue', []);
            foreach ($template as $size => $link) {
                $template[$size] = str_replace('{vendorVideoId}', $this->vendor_video_id, $link);
            }
        }

        return $template ?? [];
    }

    /**
     * Get the attribute of vendor code
     *
     * @return string
     */
    public function getVendorCodeAttribute(): string
    {
        return $this->vendor->code ?? '';
    }

    /**
     * Get the attribute of category code
     *
     * @return string
     */
    public function getCategoryCodeAttribute(): string
    {
        return $this->category->code ?? '';
    }
}
