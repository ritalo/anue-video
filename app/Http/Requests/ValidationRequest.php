<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $actionName = explode(
            '\\',
            str_replace('App\\Http\\Controllers\\', '', Route::getCurrentRoute()->getActionName())
        );
        $actionName[\count($actionName)-1] = 'validator.'.lcfirst(str_replace('Controller@', '.', $actionName[\count($actionName)-1]));
        $config = implode('.', $actionName);

        return config('validation.' . $config) ?? [];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function failedValidation(Validator $validator): void
    {
        $message = [
            'message' => '參數無效',
            'errors'  => $validator->getMessageBag()->toArray(),
            'statusCode' => Response::HTTP_UNPROCESSABLE_ENTITY
        ];

        throw (new ValidationException(
            $validator,
            new JsonResponse($message, Response::HTTP_UNPROCESSABLE_ENTITY))
        )->errorBag($this->errorBag);
    }

    /**
     * Add path parameters to be validated
     *
     * @return array
     */
    public function validationData(): array
    {
        return array_merge(parent::all(), $this->route()->parameters());
    }
}
