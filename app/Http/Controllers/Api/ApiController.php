<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host=L5_SWAGGER_CONST_HOST,
 *     basePath="/",
 *     @SWG\Info(
 *         version="v1",
 *         title="Anue Video API",
 *         description="Anue Video API",
 *         termsOfService="",
 *         @SWG\License(
 *             name="Private License",
 *             url="https://www.cnyes.com/"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="cnYES.com",
 *         url="https://www.cnyes.com/"
 *     )
 * )
 */
abstract class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Health API
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Get(
     *     path="/gateway/api/v1/health",
     *     tags={"Health API"},
     *     operationId="public.v1.ApiController.health",
     *     summary="Health API",
     *     description="Health API",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     *
     * @return \Illuminate\Http\JsonResponse|string
     */
}
