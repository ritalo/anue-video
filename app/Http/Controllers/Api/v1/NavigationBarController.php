<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use Cnyes\Helper\CnyesResponse;
use Illuminate\Support\Arr;

class NavigationBarController extends ApiController
{
    /**
     * 取得導航列設定
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Get(
     *     path="/gateway/api/v1/navigation",
     *     tags={"[Public] navigation"},
     *     operationId="getBarSettings",
     *     summary="取得導航列設定",
     *     description="取得導航列設定",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="成功"
     *     )
     * )
     */
    public function getBarSettings()
    {
        $config = config('navigation_bar.settings', []);

        return CnyesResponse::api(
            'OK',
            [
                'title' => Arr::get($config, 'title', ''),
                'image_url' => Arr::get($config, 'image_url', ''),
                'link' => Arr::get($config, 'link', ''),
            ]
        );
    }
}
