<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\ValidationRequest;
use App\Services\BannerSettingService;
use Cnyes\Helper\CnyesResponse;

class BannerController extends ApiController
{
    /**
     * 取得Banner設定
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Get(
     *     path="/gateway/api/v1/banners",
     *     tags={"[Public] Banner"},
     *     operationId="getByType",
     *     summary="取得Banner設定",
     *     description="取得Banner設定",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="type",
     *         in="query",
     *         description="Banner類型{1:首頁}",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="成功"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="參數錯誤"
     *     )
     * )
     */
    public function getByType(BannerSettingService $service, ValidationRequest $request)
    {
        return CnyesResponse::api(
            'OK',
            $service->getByType($request->get('type', 1))
        );
    }
}
