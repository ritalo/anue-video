<?php

namespace App\Http\Controllers\Api\v1\Internal;

use App\Http\Controllers\Api\ApiController;
use App\Services\BannerService;
use Cnyes\Helper\CnyesResponse;

class BannerController extends ApiController
{
    /**
     * 更新Banner
     * Content type of application/x-www-form-urlencoded only support GET and POST in Laravel.
     * Reference: https://laravel.com/docs/5.8/routing#form-method-spoofing
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Post(
     *     path="/gateway/api/v1/internal/banner",
     *     tags={"Banner"},
     *     operationId="api.v1.internal.banner.update",
     *     summary="更新Banner",
     *     description="更新Banner",
     *     consumes={"application/json", "application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="banner_id",
     *         description="Banner id",
     *         in="formData",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="title",
     *         description="主標題",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="subtitle",
     *         description="副標題",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="button_word",
     *         description="超連結按鈕文字",
     *         in="formData",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="link",
     *         description="前往超連結",
     *         in="formData",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="image_web",
     *         description="Web版圖片",
     *         in="formData",
     *         required=true,
     *         type="file"
     *     ),
     *     @SWG\Parameter(
     *         name="image_app",
     *         description="App版圖片",
     *         in="formData",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="成功"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="資料新增失敗"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="參數錯誤"
     *     )
     * )
     */
    public function update(BannerService $bannerService)
    {
        return CnyesResponse::api('OK', $bannerService->upload());
    }
}
