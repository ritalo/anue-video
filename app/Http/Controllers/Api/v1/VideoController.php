<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\ValidationRequest;
use App\Services\CategoryService;
use App\Services\VideoService;
use Cnyes\Helper\CnyesResponse;

class VideoController extends ApiController
{
    /**
     * Form request
     *
     * @var ValidationRequest $request
     */
    protected $request;

    /**
     * Create a new instance
     *
     * @param ValidationRequest $request
     */
    public function __construct(ValidationRequest $request)
    {
        $this->request = $request;
    }

    /**
     * 取得分類影片列表
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Get(
     *     path="/gateway/api/v1/video/category/{category_code}",
     *     tags={"[Public] Video"},
     *     operationId="getList",
     *     summary="取得分類影片列表",
     *     description="取得分類影片列表",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="category_code",
     *         in="path",
     *         description="類別代碼",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         description="分頁",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="一頁幾筆, 預設20筆",
     *         required=false,
     *         type="integer",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="成功"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="分類不存在"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="參數錯誤"
     *     )
     * )
     */
    public function getList($categoryCode, CategoryService $categoryService)
    {
        return CnyesResponse::api('OK', $categoryService->listVideoByCategoryCode(
            $categoryCode,
            $this->request->get('page', 1),
            $this->request->get('limit', 20)
        ));
    }

    /**
     * 取得單則影音
     * TODO: 調整為正確的路徑前綴
     *
     * @SWG\Get(
     *     path="/gateway/api/v1/video/{video_id}",
     *     tags={"[Public] Video"},
     *     operationId="getOne",
     *     summary="取得單則影音",
     *     description="取得單則影音",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="video_id",
     *         in="path",
     *         description="影音ID",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="成功"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="影音不存在"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="參數錯誤"
     *     )
     * )
     */
    public function getOne($videoId, VideoService $videoService)
    {
        return CnyesResponse::api(
            'OK',
            $videoService->getById($videoId)
        );
    }
}
