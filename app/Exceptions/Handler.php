<?php

namespace App\Exceptions;

use CnyesLogger\CnyesLogger;
use Exception;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport;

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function __construct(Container $container)
    {
        $this->dontReport = CnyesLogger::getDontReport();
        parent::__construct($container);
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if (app()->bound('CnyesLogger') && $this->shouldReport($exception)) {
            CnyesLogger::errorReportRemotely($exception);
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return parent::render($request, $exception);
        }
        if ($exception instanceof HttpException) {
            return $this->renderHttpErrors($exception);
        }

        return $this->renderUnknownErrors($exception);
    }

    /**
     * Render response with HttpException
     *
     * @param HttpException $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderHttpErrors(HttpException $exception): JsonResponse
    {
        return response()->json([
            'message' => $exception->getMessage(),
            'statusCode' => $exception->getStatusCode()
        ], $exception->getStatusCode());
    }

    /**
     * Render response with Exception
     *
     * @param Exception $exception
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderUnknownErrors(Exception $exception): JsonResponse
    {
        return response()->json([
            'message' => '未知的錯誤',
            'statusCode' => Response::HTTP_INTERNAL_SERVER_ERROR
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
