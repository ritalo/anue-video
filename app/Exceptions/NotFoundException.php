<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\MessageBag;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NotFoundException extends HttpException
{
    /**
     * Create a new NotFoundHttpException exception instance.
     *
     * @param string $message
     * @param \Illuminate\Support\MessageBag $errors
     */
    public function __construct($message, MessageBag $errors = null)
    {
        parent::__construct(Response::HTTP_NOT_FOUND, $message, $errors);
    }
}
